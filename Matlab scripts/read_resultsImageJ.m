clear all, close all

%% Writes mean values from imageJ into .coll like file
% file : path to the input file from FIJI
% outt : path to the output file
%% Input
file = 'Results.txt'; %path to th input file
out = 'exported_results.txt';%path to the output file

%% Data
dat=readmatrix(file);
frame = dat(:, 1);
means = dat(:, 2:end);
index=[];
delim=[];

%% writing
for ind = 1:length(frame)
   index=cat(1, index, strjoin(strcat(string(ind), ',')));
   delim=cat(1, delim, ";"); 
end

tab=table(index, means, delim);
writetable(tab, out, 'delimiter', ' ', 'WriteVariableNames', 0);